package ru.bullyboo.core_ui.extensions

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import kotlin.reflect.KClass

/**
 * Обычный запуск [Activity]
 * Данный метод добавляет [Activity] в конец стека [Activity]
 *
 * @param kClass - класс [Activity], которую нужно запустить
 * @param onBundle - лямбда выражение, через которое можно передать вызываемой [Activity] данные
 */
fun Activity.start(
    kClass: KClass<out Activity>,
    onBundle: ((Intent) -> Unit)? = null
) {
    val intent = Intent(this, kClass.java)
    onBundle?.invoke(intent)

    startActivity(intent)
}

/**
 * Запуск [Activity] как главного экране
 * Данный метод запускает [Activity] как одну единственную в процессе, то есть полностью
 * чистит весь стек [Activity]
 *
 * @param kClass - класс [Activity], которую нужно запустить
 * @param onBundle - лямбда выражение, через которое можно передать вызываемой [Activity] данные
 */
fun Activity.startAsRoot(
    kClass: KClass<out Activity>,
    onBundle: ((Intent) -> Unit)? = null
) {
    val intent = Intent(this, kClass.java).apply {
        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
    }
    onBundle?.invoke(intent)

    startActivity(intent)
    finish()
}

/**
 * Запуск [Activity] для получения результата
 *
 * @param requestCode - идентификатор запуска [Activity], по которому
 * будем получатть результат в [Activity.onActivityResult]
 * @param kClass - класс [Activity], которую нужно запустить
 * @param onBundle - лямбда выражение, через которое можно передать вызываемой [Activity] данные
 */
fun Activity.startForResult(
    requestCode: Int,
    kClass: KClass<out Activity>,
    onBundle: ((Intent) -> Unit)? = null
) {
    val intent = Intent(this, kClass.java)
    onBundle?.invoke(intent)

    startActivityForResult(intent, requestCode)
}

/**
 * Переход на [Activity] из стека, и чистит все [Activity], которые находились в стеке выше
 *
 * @param kClass - класс [Activity], которую нужно запустить
 */
fun Activity.startBackTo(kClass: KClass<out Activity>) {
    val intent = Intent(this, kClass.java).apply {
        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
    }
    startActivity(intent)
}

/**
 * Метод получения цвета из ресурсов
 *
 * @param colorId - идентификатор цвета в ресурасах
 */
fun Activity.getCompatColor(@ColorRes colorId: Int): Int {
    return ContextCompat.getColor(this, colorId)
}

/**
 * Метод открывающий ссылку в бразуере
 *
 * @param url - ссылка, которую нужно открыть
 */
fun Activity.startUrl(url: String) {
    val intent = Intent().apply {
        action = Intent.ACTION_VIEW
        data = Uri.parse(url)
    }
    startActivity(intent)
}