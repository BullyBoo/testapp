package ru.bullyboo.data.storages

import android.content.Context
import android.content.SharedPreferences
import ru.bullyboo.domain.storages.PreferenceStorage
import javax.inject.Inject

class PreferenceStorageImpl @Inject constructor(
    context: Context
): PreferenceStorage {

    companion object {
        private const val PREFERENCE_NAME = "Preferences"

        private const val LAST_LOADING_TIME = "last_loading_time"
    }

    private val prefs: SharedPreferences =
        context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    override fun getLastLoadingTime(): Long {
        return prefs.getLong(LAST_LOADING_TIME, 0L)
    }

    override fun setLastLoadingTime(lastLoadingTime: Long) {
        prefs.edit().putLong(LAST_LOADING_TIME, lastLoadingTime).apply()
    }
}