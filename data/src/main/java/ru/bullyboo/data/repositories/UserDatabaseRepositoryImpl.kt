package ru.bullyboo.data.repositories

import io.reactivex.Completable
import io.reactivex.Single
import ru.bullyboo.domain.database.dao.UserDao
import ru.bullyboo.domain.enities.User
import ru.bullyboo.domain.repositories.UserDatabaseRepository
import javax.inject.Inject

class UserDatabaseRepositoryImpl @Inject constructor(
    private val userDao: UserDao
): UserDatabaseRepository {

    override fun getUsers(): Single<List<User>> {
        return userDao.getUsers()
    }

    override fun save(list: List<User>): Completable {
        return userDao.saveUsers(list)
    }
}