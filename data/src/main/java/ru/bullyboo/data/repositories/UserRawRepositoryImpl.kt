package ru.bullyboo.data.repositories

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Single
import ru.bullyboo.domain.enities.User
import ru.bullyboo.domain.providers.RawProvider
import ru.bullyboo.domain.repositories.UserRawRepository
import javax.inject.Inject

class UserRawRepositoryImpl @Inject constructor(
    private val rawProvider: RawProvider,
    private val gson: Gson
): UserRawRepository {

    private val userTypeToken = object: TypeToken<List<User>>(){}.type

    override fun getUsers(vararg rawIds: Int): Single<List<User>> {
        return Single.fromCallable {
            rawIds.map { rawProvider.getSource(it) }
                .map { gson.fromJson<List<User>>(it, userTypeToken) }
                .flatten()
        }
    }
}