package ru.bullyboo.data.providers

import android.content.Context
import ru.bullyboo.domain.providers.RawProvider
import javax.inject.Inject

class RawProviderImpl @Inject constructor(
    private val context: Context
): RawProvider {

    override fun getSource(rawId: Int): String {
        return context.resources.openRawResource(rawId)
            .bufferedReader()
            .readText()
    }
}