package ru.bullyboo.application.dagger.modules

import dagger.Binds
import dagger.Module
import ru.bullyboo.data.providers.RawProviderImpl
import ru.bullyboo.domain.providers.RawProvider
import javax.inject.Singleton

@Module
interface ProvidersModule {

    @Binds
    @Singleton
    fun provideRawProvider(impl: RawProviderImpl): RawProvider
}