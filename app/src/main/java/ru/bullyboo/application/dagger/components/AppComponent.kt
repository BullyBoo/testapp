package ru.bullyboo.application.dagger.components

import dagger.Component
import ru.bullyboo.application.dagger.modules.*
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        GsonModule::class,
        StorageModule::class,
        ProvidersModule::class,
        DatabaseModule::class
    ]
)
interface AppComponent {

    fun mainBuilder(): MainComponent.Builder

}