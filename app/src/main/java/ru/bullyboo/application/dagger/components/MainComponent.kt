package ru.bullyboo.application.dagger.components

import dagger.Subcomponent
import ru.bullyboo.application.dagger.annotations.MainScope
import ru.bullyboo.application.dagger.modules.main.MainModule
import ru.bullyboo.application.ui.main.MainActivity

@MainScope
@Subcomponent(
    modules = [
        MainModule::class
    ]
)
interface MainComponent {

    fun inject(entity: MainActivity)

    @Subcomponent.Builder
    interface Builder {

        fun build(): MainComponent
    }
}