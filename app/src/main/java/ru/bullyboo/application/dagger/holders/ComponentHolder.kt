package ru.bullyboo.application.dagger.holders

interface ComponentHolder<Component> {

    fun provide(): Component
}