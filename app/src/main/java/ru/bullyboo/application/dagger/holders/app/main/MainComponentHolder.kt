package ru.bullyboo.application.dagger.holders.app.main

import ru.bullyboo.application.dagger.Dagger
import ru.bullyboo.application.dagger.components.AppComponent
import ru.bullyboo.application.dagger.components.MainComponent
import ru.bullyboo.application.dagger.holders.app.AppComponentHolder
import ru.bullyboo.application.dagger.holders.ChildComponentHolder

class MainComponentHolder(
    app: AppComponentHolder
): ChildComponentHolder<AppComponent, MainComponent>(
    app
) {

    override fun build(parent: AppComponent): MainComponent {
        return Dagger.app.provide().mainBuilder().build()
    }
}