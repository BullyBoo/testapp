package ru.bullyboo.application.dagger.modules

import dagger.Binds
import dagger.Module
import ru.bullyboo.data.storages.PreferenceStorageImpl
import ru.bullyboo.domain.storages.PreferenceStorage
import javax.inject.Singleton

@Module
interface StorageModule{

    @Binds
    @Singleton
    fun providePreferenceStorage(impl: PreferenceStorageImpl): PreferenceStorage
}