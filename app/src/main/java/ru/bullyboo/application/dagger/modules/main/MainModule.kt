package ru.bullyboo.application.dagger.modules.main

import dagger.Binds
import dagger.Module
import ru.bullyboo.application.dagger.annotations.MainScope
import ru.bullyboo.data.repositories.UserDatabaseRepositoryImpl
import ru.bullyboo.data.repositories.UserRawRepositoryImpl
import ru.bullyboo.domain.interactors.MainInteractor
import ru.bullyboo.domain.interactors.impl.MainInteractorImpl
import ru.bullyboo.domain.repositories.UserDatabaseRepository
import ru.bullyboo.domain.repositories.UserRawRepository

@Module
interface MainModule {

    @Binds
    @MainScope
    fun provideMainInteractor(impl: MainInteractorImpl): MainInteractor

    @Binds
    @MainScope
    fun provideUserRawRepository(impl: UserRawRepositoryImpl): UserRawRepository

    @Binds
    @MainScope
    fun provideUserDatabaseRepository(impl: UserDatabaseRepositoryImpl): UserDatabaseRepository
}