package ru.bullyboo.application.dagger.modules

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ru.bullyboo.domain.database.AppDatabase
import ru.bullyboo.domain.database.dao.UserDao
import javax.inject.Singleton

@Module
class DatabaseModule {

    companion object {

        private const val DATABASE_NAME = "user.db"
    }

    @Singleton
    @Provides
    fun provideAppDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
            .build()
    }

    @Singleton
    @Provides
    fun provideUserDao(database: AppDatabase): UserDao {
        return database.userDao()
    }
}