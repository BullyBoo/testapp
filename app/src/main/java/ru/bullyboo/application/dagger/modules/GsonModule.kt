package ru.bullyboo.application.dagger.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import ru.bullyboo.domain.converters.CalendarConverter
import ru.bullyboo.domain.converters.UserTemperamentConverter
import ru.bullyboo.domain.enities.User
import java.util.*
import javax.inject.Singleton

@Module
class GsonModule {

    @Provides
    @Singleton
    fun provideGson(): Gson =
        GsonBuilder()
            .registerTypeAdapter(User.Temperament::class.java, UserTemperamentConverter())
            .registerTypeHierarchyAdapter(Calendar::class.java, CalendarConverter())

            .create()
}