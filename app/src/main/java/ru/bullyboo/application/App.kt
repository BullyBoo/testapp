package ru.bullyboo.application

import android.app.Application
import ru.bullyboo.application.dagger.Dagger

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        Dagger.init(this)
    }
}