package ru.bullyboo.application.ui.main

import ru.bullyboo.application.ui.base.BaseView
import ru.bullyboo.domain.enities.User

interface MainView: BaseView {

    fun submit(list: List<User>)

    fun setRecyclerVisibility(isVisible: Boolean)

    fun hideRefreshing()
}