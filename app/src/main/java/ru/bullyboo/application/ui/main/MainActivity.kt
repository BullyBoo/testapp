package ru.bullyboo.application.ui.main

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_main.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import ru.bullyboo.application.R
import ru.bullyboo.application.dagger.Dagger
import ru.bullyboo.application.ui.base.BaseActivity
import ru.bullyboo.application.ui.details.DetailsActivity
import ru.bullyboo.application.ui.main.adapter.MainAdapter
import ru.bullyboo.core.enums.DateMask
import ru.bullyboo.core.utils.DateConverter
import ru.bullyboo.core_ui.adapters.dividers.Divider
import ru.bullyboo.core_ui.extensions.makeGone
import ru.bullyboo.core_ui.extensions.makeVisible
import ru.bullyboo.core_ui.extensions.start
import ru.bullyboo.domain.enities.User
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

class MainActivity: BaseActivity(),
    MainView,
    MainAdapter.OnActionListener {

    @Inject
    internal lateinit var provider: Provider<MainPresenter>

    @InjectPresenter
    internal lateinit var presenter: MainPresenter

    @ProvidePresenter
    fun provide(): MainPresenter = provider.get()

    private val defaultLocale = Locale.getDefault()

    private lateinit var adapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        Dagger.app.main.provide().inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = MainAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(Divider(this))

        swipeRefreshLayout.setOnRefreshListener {
            presenter.onRefresh()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchItem: MenuItem? = menu?.findItem(R.id.action_search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView: SearchView? = searchItem?.actionView as? SearchView

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        searchView?.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                /* nothing */
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                presenter.onTextChanged(newText.orEmpty())
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun submit(list: List<User>) {
        adapter.submitList(list)
    }

    override fun setRecyclerVisibility(isVisible: Boolean) {
        recyclerView.isVisible = isVisible
    }

    override fun hideRefreshing() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showLoading() {
        progressBar.makeVisible()
    }

    override fun hideLoading() {
        progressBar.makeGone()
    }

    override fun onClickUser(user: User) {
        start(DetailsActivity::class){
            val temperament = user.temperament.name
                .toLowerCase(defaultLocale)
                .capitalize(defaultLocale)

            val startPeriod = DateConverter.parse(user.educationPeriod.startCalendar, DateMask.DD_MM_YYYY)
            val endPeriod = DateConverter.parse(user.educationPeriod.endCalendar, DateMask.DD_MM_YYYY)

            val datePeriod = "$startPeriod - $endPeriod"
            it.putExtra(DetailsActivity.USER_NAME, user.name)
            it.putExtra(DetailsActivity.USER_PHONE, user.phone)
            it.putExtra(DetailsActivity.USER_TEMPERAMENT, temperament)
            it.putExtra(DetailsActivity.USER_DATE_PERIOD, datePeriod)
            it.putExtra(DetailsActivity.USER_BIOGRAPHY, user.biography)
        }
    }
}