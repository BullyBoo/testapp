package ru.bullyboo.application.ui.main.adapter

import androidx.recyclerview.widget.DiffUtil
import ru.bullyboo.domain.enities.User

class UserDiffCallback(
    private val oldList: List<User>,
    private val newList: List<User>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
                && oldList[oldItemPosition].biography == newList[newItemPosition].biography
                && oldList[oldItemPosition].educationPeriod == newList[newItemPosition].educationPeriod
                && oldList[oldItemPosition].phone == newList[newItemPosition].phone
                && oldList[oldItemPosition].height == newList[newItemPosition].height
                && oldList[oldItemPosition].temperament == newList[newItemPosition].temperament
                && oldList[oldItemPosition].name == newList[newItemPosition].name
    }
}