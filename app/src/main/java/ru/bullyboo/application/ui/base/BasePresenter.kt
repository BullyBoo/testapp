package ru.bullyboo.application.ui.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import moxy.MvpPresenter

/**
 * Базовый класс Presenter, реализующий основной функционал, который нужен для
 * правильной работы Presenter
 */
abstract class BasePresenter<V: BaseView>: MvpPresenter<V>() {

    private val compositeDisposable by lazy { CompositeDisposable() }

    override fun onDestroy() {
        super.onDestroy()
        dispose()
    }

    /**
     * Метод, позволяющий привязать Disposable к жизненному циклу BasePresenter
     * Таким образом происходит автоматическая отписка всех подписок Presenter,
     * при разрушении Presenter
     */
    fun Disposable.disposeOnPresenterDestroy(): Disposable {
        compositeDisposable.add(this)
        return this
    }

    private fun dispose() {
        compositeDisposable.dispose()
    }
}