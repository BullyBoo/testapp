package ru.bullyboo.application.ui.details

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details.*
import ru.bullyboo.application.R
import ru.bullyboo.application.ui.base.BaseActivity

class DetailsActivity: BaseActivity() {

    companion object {

        const val USER_NAME = "user_name"
        const val USER_PHONE = "user_phone"
        const val USER_TEMPERAMENT = "user_temperament"
        const val USER_DATE_PERIOD = "user_date_period"
        const val USER_BIOGRAPHY = "user_biography"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        nameText.text = intent.getStringExtra(USER_NAME)
        phoneText.text = intent.getStringExtra(USER_PHONE)
        temperamentText.text = intent.getStringExtra(USER_TEMPERAMENT)
        datePeriodText.text = intent.getStringExtra(USER_DATE_PERIOD)
        biographyText.text = intent.getStringExtra(USER_BIOGRAPHY)
    }
}