package ru.bullyboo.application.ui.base

import androidx.annotation.StringRes
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

interface BaseView: MvpView {

    @StateStrategyType(SkipStrategy::class)
    fun showToast(@StringRes stringId: Int)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoading() = Unit

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun hideLoading() = Unit
}