package ru.bullyboo.application.ui.main

import io.reactivex.Single
import moxy.InjectViewState
import ru.bullyboo.application.R
import ru.bullyboo.application.ui.base.BasePresenter
import ru.bullyboo.core.utils.log
import ru.bullyboo.core_ui.extensions.schedulerIoToMain
import ru.bullyboo.domain.enities.User
import ru.bullyboo.domain.interactors.MainInteractor
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor(
    private val interactor: MainInteractor
): BasePresenter<MainView>() {

    private val rawIds = intArrayOf(
        R.raw.source1,
        R.raw.source2,
        R.raw.source3
    )

    private val list: ArrayList<User> = arrayListOf()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        startUserUploading(interactor.getUsers(rawIds))
    }

    fun onRefresh(){
        startUserUploading(interactor.refreshUsers(rawIds))
    }

    fun onTextChanged(text: String){
        viewState.submit(
            list.filter {
                it.name.startsWith(text, ignoreCase = true)
            }
        )
    }

    private fun startUserUploading(single: Single<List<User>>){

        viewState.setRecyclerVisibility(false)
        viewState.showLoading()

        single.schedulerIoToMain()
            .subscribe({
                viewState.setRecyclerVisibility(true)
                viewState.hideLoading()
                viewState.hideRefreshing()

                list.clear()
                list.addAll(it)

                viewState.submit(list)
            }, {
                viewState.setRecyclerVisibility(true)
                viewState.hideLoading()
                viewState.hideRefreshing()
                viewState.showToast(R.string.error)

                log(it)
            })
            .disposeOnPresenterDestroy()
    }
}