package ru.bullyboo.application.ui.main.adapter

import androidx.recyclerview.widget.DiffUtil
import kotlinx.android.synthetic.main.item_user.view.*
import ru.bullyboo.application.R
import ru.bullyboo.core_ui.adapters.FastAdapter
import ru.bullyboo.domain.enities.User

class MainAdapter(
    private val listener: OnActionListener
): FastAdapter<User>() {

    override fun getLayoutId(viewType: Int) = R.layout.item_user

    override fun onBind(holder: ViewHolder, model: User, viewType: Int) {
        with(holder.itemView) {
            nameText.text = model.name
            phoneText.text = model.phone
            heightText.text = model.height.toString()

            setOnClickListener { listener.onClickUser(model) }
        }
    }

    fun submitList(model: List<User>) {
        val diffCallback = UserDiffCallback(list, model)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        list.clear()
        list.addAll(model)
        diffResult.dispatchUpdatesTo(this)
    }

    interface OnActionListener{

        fun onClickUser(user: User)
    }
}