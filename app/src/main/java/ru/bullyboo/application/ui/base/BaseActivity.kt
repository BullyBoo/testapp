package ru.bullyboo.application.ui.base

import com.google.android.material.snackbar.Snackbar
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import moxy.MvpAppCompatActivity

/**
 * Базовый класс Activity, реализующий основные функции для работы экрана
 *
 * Данный класс наследуется от MvpAppCompatActivity, чем реализует автоматическое
 * соединение Model View Presenter
 */
abstract class BaseActivity: MvpAppCompatActivity(), BaseView {

    private val compositeDisposable by lazy { CompositeDisposable() }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    override fun showToast(stringId: Int) {
        val snackBar = Snackbar.make(findViewById(android.R.id.content), getString(stringId), Snackbar.LENGTH_LONG)
        snackBar.setText(stringId)
        snackBar.show()
    }

    /**
     * Метод, позволяющий привязать Disposable к жизненному циклу Activity
     * Таким образом происходит автоматическая отписка всех подписок Activity,
     * при разрушении Activity
     */
    fun Disposable.disposeOnActivityDestroy(): Disposable {
        compositeDisposable.add(this)
        return this
    }
}