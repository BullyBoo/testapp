package ru.bullyboo.domain.providers

interface RawProvider {

    fun getSource(rawId: Int): String
}