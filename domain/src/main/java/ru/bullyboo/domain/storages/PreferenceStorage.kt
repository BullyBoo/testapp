package ru.bullyboo.domain.storages

interface PreferenceStorage {

    fun getLastLoadingTime(): Long

    fun setLastLoadingTime(lastLoadingTime: Long)
}