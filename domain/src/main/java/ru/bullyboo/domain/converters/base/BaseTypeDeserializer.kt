package ru.bullyboo.domain.converters.base

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

abstract class BaseTypeDeserializer<Model>: JsonDeserializer<Model>{

    abstract fun deserialize(
        text: String,
    ): Model

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): Model {
        return deserialize(json.asString)
    }
}