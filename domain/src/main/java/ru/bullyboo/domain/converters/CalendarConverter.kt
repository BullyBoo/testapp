package ru.bullyboo.domain.converters

import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import ru.bullyboo.core.utils.DateConverter
import ru.bullyboo.domain.converters.base.BaseTypeConverter
import java.util.*

class CalendarConverter: BaseTypeConverter<Calendar>(){

    override fun deserialize(text: String): Calendar {
        return Calendar.getInstance().apply {
            timeInMillis = DateConverter.parse(text)
        }
    }

    override fun serialize(source: Calendar): JsonElement {
        return JsonPrimitive(DateConverter.parse(source))
    }
}