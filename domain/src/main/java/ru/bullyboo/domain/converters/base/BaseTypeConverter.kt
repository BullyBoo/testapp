package ru.bullyboo.domain.converters.base

import com.google.gson.*
import java.lang.reflect.Type

abstract class BaseTypeConverter<Model>: JsonDeserializer<Model>, JsonSerializer<Model>{

    abstract fun deserialize(
        text: String,
    ): Model

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): Model {
        return deserialize(json.asString)
    }

    abstract fun serialize(
        source: Model,
    ): JsonElement

    override fun serialize(
        src: Model,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement {
        return serialize(src)
    }
}