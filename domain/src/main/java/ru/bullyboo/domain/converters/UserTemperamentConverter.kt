package ru.bullyboo.domain.converters

import ru.bullyboo.domain.converters.base.BaseTypeDeserializer
import ru.bullyboo.domain.enities.User

class UserTemperamentConverter: BaseTypeDeserializer<User.Temperament>(){

    override fun deserialize(text: String): User.Temperament {
        return User.Temperament.get(text)
    }
}