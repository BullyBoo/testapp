package ru.bullyboo.domain.enities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import ru.bullyboo.domain.database.converters.CalendarConverter
import ru.bullyboo.domain.database.converters.UserTemperamentConverter
import java.util.*

@Entity
@TypeConverters(
    value = [
        UserTemperamentConverter::class,
        CalendarConverter::class
    ]
)
data class User(
    @PrimaryKey
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("height") val height: Float,
    @SerializedName("biography") val biography: String,
    @SerializedName("temperament") val temperament: Temperament,
    @Embedded
    @SerializedName("educationPeriod") val educationPeriod: Period
) {

    data class Period(
        @SerializedName("start") val startCalendar: Calendar,
        @SerializedName("end") val endCalendar: Calendar
    )

    enum class Temperament {
        MELANCHOLIC,
        PHLEGMATIC,
        SANGUINE,
        CHOLERIC;

        companion object {

            fun get(name: String) = values().first { it.name.equals(name, true) }
        }
    }
}