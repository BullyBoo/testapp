package ru.bullyboo.domain.interactors.impl

import io.reactivex.Single
import ru.bullyboo.domain.enities.User
import ru.bullyboo.domain.interactors.MainInteractor
import ru.bullyboo.domain.repositories.UserDatabaseRepository
import ru.bullyboo.domain.repositories.UserRawRepository
import ru.bullyboo.domain.storages.PreferenceStorage
import javax.inject.Inject

class MainInteractorImpl @Inject constructor(
    private val preferenceStorage: PreferenceStorage,
    private val userRawRepository: UserRawRepository,
    private val userDatabaseRepository: UserDatabaseRepository
): MainInteractor {

    companion object {

        private const val MAX_DIFF = 1 * 60 * 1_000
    }

    override fun getUsers(rawIds: IntArray): Single<List<User>> {
        return if(needRefresh()){
            refreshUsers(rawIds)
        } else {
            userDatabaseRepository.getUsers()
        }
    }

    override fun refreshUsers(rawIds: IntArray): Single<List<User>> {
        return userRawRepository.getUsers(rawIds)
            .flatMap {
                userDatabaseRepository.save(it)
                    .andThen(Single.just(it))
            }
            .doOnSuccess {
                preferenceStorage.setLastLoadingTime(System.currentTimeMillis())
            }
    }

    private fun needRefresh() =
            System.currentTimeMillis() - preferenceStorage.getLastLoadingTime() > MAX_DIFF
}