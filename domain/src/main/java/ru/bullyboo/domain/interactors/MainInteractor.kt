package ru.bullyboo.domain.interactors

import io.reactivex.Single
import ru.bullyboo.domain.enities.User

interface MainInteractor {

    fun getUsers(rawIds: IntArray): Single<List<User>>

    fun refreshUsers(rawIds: IntArray): Single<List<User>>

}