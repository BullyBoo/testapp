package ru.bullyboo.domain.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.bullyboo.domain.database.dao.UserDao
import ru.bullyboo.domain.enities.User

@Database(
    entities = [
        User::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
}