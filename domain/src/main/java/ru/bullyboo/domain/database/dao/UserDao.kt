package ru.bullyboo.domain.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import ru.bullyboo.domain.enities.User

@Dao
abstract class UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveUsers(users: List<User>): Completable

    @Query("SELECT * FROM user")
    abstract fun getUsers(): Single<List<User>>
}