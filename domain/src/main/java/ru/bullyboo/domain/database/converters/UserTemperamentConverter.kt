package ru.bullyboo.domain.database.converters

import androidx.room.TypeConverter
import ru.bullyboo.domain.enities.User

object UserTemperamentConverter {

    @JvmStatic
    @TypeConverter
    fun toDb(type: User.Temperament): String {
        return type.name
    }

    @JvmStatic
    @TypeConverter
    fun fromDb(value: String): User.Temperament {
        return User.Temperament.get(value)
    }
}