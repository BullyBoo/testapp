package ru.bullyboo.domain.database.converters

import androidx.room.TypeConverter
import ru.bullyboo.domain.enities.User
import java.util.*

object CalendarConverter {

    @JvmStatic
    @TypeConverter
    fun toDb(calendar: Calendar): Long {
        return calendar.timeInMillis
    }

    @JvmStatic
    @TypeConverter
    fun fromDb(value: Long): Calendar {
        return Calendar.getInstance().apply {
            timeInMillis = value
        }
    }
}