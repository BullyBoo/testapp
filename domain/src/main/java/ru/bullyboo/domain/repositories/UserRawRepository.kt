package ru.bullyboo.domain.repositories

import io.reactivex.Single
import ru.bullyboo.domain.enities.User

interface UserRawRepository {

    fun getUsers(rawIds: IntArray): Single<List<User>>
}