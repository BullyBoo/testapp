package ru.bullyboo.domain.repositories

import io.reactivex.Completable
import io.reactivex.Single
import ru.bullyboo.domain.enities.User

interface UserDatabaseRepository {

    fun getUsers(): Single<List<User>>

    fun save(list: List<User>): Completable
}